/**
 * @file   data.hpp
 * @Author Mihai Stef
 *
 * Contains a wrapper for data that is modified during transaction that stores last modified value of the variable and erases the data type.
 * In order to keep track of modified data of different type during a transaction data type must be erased when logged.
 */

#ifndef DATA_HPP_
#define DATA_HPP_

#include "stm_types.hpp"
#include <type_traits>

namespace stm
{

namespace details
{

/**
 * Base class of data wrapper.
 */
struct DataBase
{
    template<class T> void write( const T& v );
    template<class T> T getValue();
    virtual void save( const Address& a ) = 0;
    virtual ~DataBase()
    {
    }
};

/**
 * Wrapper for data modified by a transaction.
 */
template<class T>
class Data: public DataBase
{
public:

    Data( const Data& ) = delete;
    Data& operator=( const Data& ) = delete;

    explicit Data( const T& v );

    virtual void save( const Address& a );

    void write( const T& v );

    T getValue();
private:
    T value; //!< Holder of the modified value
};

template<class T> Data<T>::Data( const T& v ) :
        value( v )
{
    static_assert( std::is_copy_constructible<T>::value, "T must be copy constructible" );
}

template<class T> /*virtual*/void Data<T>::save( const Address& a )
{
    *( reinterpret_cast<T*>( a ) ) = value;
}

template<class T> void Data<T>::write( const T& v )
{
    value = v;
}

template<class T> T Data<T>::getValue()
{
    return value;
}

template<class T> void DataBase::write( const T& v )
{
    static_cast<Data<T>&>( *this ).write( v );
}

template<class T> T DataBase::getValue()
{
    return static_cast<Data<T>&>( *this ).getValue();
}

} // namespace details

} //namespace stm

#endif /* DATA_HPP_ */
