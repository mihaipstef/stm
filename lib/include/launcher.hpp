/**
 * @file   launcher.hpp
 * @Author Mihai Stef
 *
 * Contains utilities to launch a transition
 */

#ifndef LAUNCHER_HPP_
#define LAUNCHER_HPP_

#include "transaction.hpp"

namespace stm
{

class TransactionLauncher
{
public:
    TransactionLauncher() = default;
    TransactionLauncher( const TransactionLauncher& ) = delete;
    TransactionLauncher( TransactionLauncher&& ) = delete;
    TransactionLauncher& operator=( const TransactionLauncher& ) = delete;
    TransactionLauncher& operator=( TransactionLauncher&& ) = delete;
    ~TransactionLauncher() = default;

    TransactionLauncher& operator=( const std::function<void( stm::Transaction& )>& func )
    {
        stm::run_transaction( func );
        return *this;
    }
};

} // namespace stm

#endif
