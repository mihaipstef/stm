/**
 * @file   data.hpp
 * @Author Mihai Stef
 *
 * Contains the global memory metadata structure.
 */

#ifndef MEM_HPP_
#define MEM_HPP_

#include <cstdint>
#include <map>
#include <memory>
#include "stm_types.hpp"
#include "util.hpp"

namespace stm
{

namespace details
{

/**
 * Metadata for a tracked object
 */
struct MemEntryMetadata
{
    MemEntryMetadata();
    SequenceId sequence;
    util::SpinLock lock;
}; // MemEntryMetadata

/**
 * Stores meta data for all tracked memory (objects).
 */
class Memory
{
public:

    Memory() = default;
    Memory( const Memory& m ) = delete;
    Memory& operator=( const Memory& m ) = delete;

    static Memory& get();
    std::shared_ptr<MemEntryMetadata> getMetaData( const Address& a );
    void add( const Address& a );
    void remove( const Address& a );
    void reset();

private:
    std::map<Address, std::shared_ptr<MemEntryMetadata>> mem;
}; // Memory

}// namespace details

} // namespace stm

#endif /* MEM_HPP_ */
