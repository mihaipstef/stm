/**
 * @file   stm.hpp
 * @Author Mihai Stef
 *
  * Implementation uses the algorithm proposed in D. Dice, O. Shalev and N. Shavit, "Transactional Locking II",
 * Proceedings of the 20th international conference on Distributed Computing (DISC), 2006
 *
 */

#ifndef STM_HPP_
#define STM_HPP_

#include <functional>
#include "launcher.hpp"

#define TRANSACTION TRANSACTION_IMPL(__COUNTER__)

#define TRANSACTION_IMPL(id) \
    stm::TransactionLauncher launcher##id; \
    launcher##id = [&]( stm::Transaction& t )

namespace stm
{
} // namespace stm

#endif /* STM_HPP_ */
