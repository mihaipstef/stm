/**
 * @file   transaction.hpp
 * @Author Mihai Stef
 *
 * Contains the declaration of the transaction handle and template definitions.
 * Implementation uses the algorithm proposed in D. Dice, O. Shalev and N. Shavit, "Transactional Locking II",
 * Proceedings of the 20th international conference on Distributed Computing (DISC), 2006
 *
 */

 #ifndef TRANSACTION_HPP_
 #define TRANSACTION_HPP_

 #include "data.hpp"
 #include <functional>
 #include <map>
 #include "mem.hpp"
 #include <memory>
 #include <set>
 #include "stm_types.hpp"
 #include <type_traits>
 #include "util.hpp"
 #include <utility>

namespace stm
{

namespace details
{

/**
 * Data information to be logged on write(store) operations
 */
struct WriteDataInfo
{
    std::unique_ptr<DataBase> data; //!< Keeps track of the modified value
    std::shared_ptr<MemEntryMetadata> meta; //!< Cache the corresponding memory entry meta data
};

}

/**
 * Transaction handle.
 * Keeps track of reads(loads) and writes(stores) performed during a transaction and commits the modifications.
 */
class Transaction
{
public:
    explicit Transaction( const SequenceId& _id );
    ~Transaction();

    Transaction( const Transaction& t ) = delete;
    Transaction& operator=( const Transaction& t ) = delete;

    template<class T>
    util::Maybe<T> read( const Address& a );

    template<class T>
    void write( const Address& a, const T& v );

    void setConflict( bool c );

    bool getConflict() const;

    bool commit();

private:
    SequenceId read_id; //!< Sequence id used by read(load) operations
    std::map<Address, std::shared_ptr<details::MemEntryMetadata>> reads; //!< Store all the reads(loads) during transaction
    std::map<Address, details::WriteDataInfo> writes; //!< Store all the writes(stores) during transaction
    bool conflict_detected; //!< Indicates if a conflict was detected
};

/**
 * Executes a given transaction.
 */
void run_transaction( const std::function<void( Transaction& )>& func );

/**
 * Logs a read(load) from a given shared variable.
 */
template<class T>
util::Maybe<T> Transaction::read( const Address& a )
{
    util::Maybe<T> result;

    std::shared_ptr<details::MemEntryMetadata> meta = details::Memory::get().getMetaData( a );

    // A conflict exists if the read location is locked for writing or if location was already written after transaction was created
    conflict_detected = conflict_detected || meta->lock.check() || ( read_id < meta->sequence );

    if( !conflict_detected )
    {
        auto it = writes.find( a );
        if( it != writes.end() )
        {
            result = it->second.data->getValue<T>();
        }
        reads.insert( std::make_pair( a, meta ) );
    }
    return result;
}

/**
 * Logs a write(store) to a given shared variable.
 */
template<class T>
void Transaction::write( const Address& a, const T& v )
{
    if( !conflict_detected )
    {
        auto it = writes.find( a );
        if( it != writes.end() )
        {
            it->second.data->write( v );
        }
        else
        {
            details::WriteDataInfo info
            { std::unique_ptr < details::DataBase > ( new details::Data<T>( v ) ), details::Memory::get().getMetaData( a ) };
            // Transaction owns all tracked modified data.
            writes.insert( std::make_pair( a, std::move( info ) ) );
        }
    }
}

}

#endif
