/**
 * @file   util.hpp
 * @Author Mihai Stef
 *
 * Contains utility classes to support stm implementation.
 */

#ifndef UTIL_HPP_
#define UTIL_HPP_

#include <atomic>
#include <experimental/optional>

namespace util
{

template<class T>
using Maybe = std::experimental::optional<T>;

/**
 * Spin lock implementation.
 */
class SpinLock
{
public:
    SpinLock();
    bool check() const;
    void acquire();
    void release();
private:
    std::atomic<bool> flag;
};

/**
 * Atomic counter implementation
 */
template<class T>
class AtomicCounter
{

private:
    std::atomic<T> counter;

public:

    AtomicCounter() :
            counter( 0 )
    {
    }

    AtomicCounter( const AtomicCounter& ) = delete;
    AtomicCounter& operator=( const AtomicCounter& ) = delete;

    T inc()
    {
        return ++counter;
    }

    T dec()
    {
        return --counter;
    }

    T getValue() const
    {
        return counter.load();
    }

};

} // namespace util

#endif /* UTIL_HPP_ */
