/**
 * @file   var.hpp
 * @Author Mihai Stef
 *
 * Contains the declaration of the shared data wrapper and template definitions.
 *
 */

#ifndef VAR_HPP_
#define VAR_HPP_

#include "data.hpp"
#include "mem.hpp"
#include "stm_types.hpp"
#include <type_traits>
#include "util.hpp"
#include <utility>
#include "transaction.hpp"

namespace stm
{

/**
 * Shared variable.
 */
template<class T>
class Var
{

    static_assert( std::is_copy_constructible<T>::value, "T must be copy constructible" );

private:
    T value;

public:

    Var() = delete;
    Var( const Var<T>& ) = delete;
    Var<T>& operator=( const Var<T>& ) = delete;

    explicit Var( const T& v ) :
            value( v )
    {
        details::Memory::get().add( reinterpret_cast<Address>( &value ) );
    }

    explicit Var( Var<T> && rv ) :
            value( std::move( rv.value ) )
    {
    }

    ~Var()
    {
        details::Memory::get().remove( reinterpret_cast<Address>( &value ) );
    }

    T load( Transaction& t )
    {
        util::Maybe<T> v = t.read<T>( reinterpret_cast<Address>( &value ) );
        if( v )
        {
            return v.value();
        }
        return value;
    }

    void store( const T& v, Transaction& t )
    {
        t.write( reinterpret_cast<Address>( &value ), v );
    }

    T getValue()
    {
        return value;
    }
};

}

#endif
