#include "mem.hpp"

namespace stm
{

namespace details
{
MemEntryMetadata::MemEntryMetadata() :
        sequence( 0 )
{
}

Memory& Memory::get()
{
    static Memory mem;
    return mem;
}

std::shared_ptr<MemEntryMetadata> Memory::getMetaData( const Address& a )
{
    auto it = mem.find( a );
    if( it != mem.end() )
    {
        return it->second;
    }
    return std::shared_ptr<MemEntryMetadata>();
}

void Memory::add( const Address& a )
{
    mem.insert( std::make_pair( a, std::make_shared<MemEntryMetadata>() ) );
}

void Memory::remove( const Address& a )
{
    mem.erase( a );
}

void Memory::reset()
{
    for( auto&& m : mem )
    {
        m.second->sequence = 0;
    }
}

} // namespace details

} // namespace stm
