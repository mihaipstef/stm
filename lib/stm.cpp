#include "stm.hpp"
#include <functional>
#include <iostream>
namespace stm
{

namespace details
{

static util::AtomicCounter<int> activeTransactionsCount; //!< Keeps track of the number of active transactions.

/**
 * Generates atomically sequence identifiers
 */
struct SequenceCounter
{
    static SequenceId getId();
    static bool overflowed;
};

bool SequenceCounter::overflowed( false );

SequenceId SequenceCounter::getId()
{
    static SequenceId transactionCounter;
    static SequenceId lastId;
    static util::SpinLock sequenceIdLock;

    sequenceIdLock.acquire();
    SequenceId id = ++transactionCounter;
    if( !overflowed )
    {
        if( lastId > id )
        {
            overflowed = true;
        }
        lastId = id;
    }
    sequenceIdLock.release();

    return id;
}

} // namespace details

Transaction::Transaction( const SequenceId& _id ) :
        read_id( _id ), conflict_detected( false )
{
    details::activeTransactionsCount.inc();
}

Transaction::~Transaction()
{
    details::activeTransactionsCount.dec();
}

bool Transaction::getConflict() const
{
    return conflict_detected;
}

/**
 * Performs a commit.
 */
bool Transaction::commit()
{
    bool result( true );
    bool wresult( true );

    if( !conflict_detected )
    {
        //Lock all writing locations
        for( auto&& w : writes )
        {
            w.second.meta->lock.acquire();
        }

        //Check if variables read in transaction weren't updated by other transactions
        for( auto&& a : reads )
        {
            result = result && a.second && ( read_id >= a.second->sequence );
        }

        //Validate writes and commit
        if( result )
        {
            //Get a new sequence id...
            SequenceId writeId = details::SequenceCounter::getId();
            //... and validate the writes.
            for( auto&& w : writes )
            {
                wresult = wresult && w.second.meta && ( writeId > w.second.meta->sequence );
            }

            //If all checks hold...
            if( wresult )
            {
                //...commit the modifications.
                for( auto&& w : writes )
                {
                    w.second.data->save( w.first );
                    w.second.meta->sequence = writeId;
                }
            }
        }

        //Release locks
        for( auto&& w : writes )
        {
            w.second.meta->lock.release();
        }
    }
    return !conflict_detected && wresult && result;
}

/**
 * Executes a transaction until succeeds
 */
void run_transaction( const std::function<void( Transaction& )>& func )
{

    static util::SpinLock resetLock;
    static bool needResetMemory( false );

    static thread_local auto do_transaction = [&]( auto&& id )
    {
        Transaction t( id );
        func( t );
        return t.commit();
    };

    // Try to execute the transaction until succeed.
    while( true )
    {
        SequenceId id( details::SequenceCounter::getId() );

        //If sequence counter overflowed...
        if( details::SequenceCounter::overflowed )
        {
            resetLock.acquire();
            if( !needResetMemory && ( details::SequenceCounter::overflowed ) )
            {
                needResetMemory = true;
            }
            resetLock.release();
            //...wait all active transactions to finish.
            while( details::activeTransactionsCount.getValue() > 0 )
                ;
            //...reset all sequence ids in memory...
            resetLock.acquire();
            if( needResetMemory )
            {
                details::Memory::get().reset();
                needResetMemory = false;
                details::SequenceCounter::overflowed = false;
            }
            resetLock.release();
        }
        //If sequence counter is ok...
        else
        {
            //... execute the transaction.
            if( do_transaction( id ) )
            {
                break;
            }
        }
    }
}

} // namespace stm
