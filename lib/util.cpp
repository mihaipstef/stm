#include "util.hpp"

namespace util
{
SpinLock::SpinLock() :
        flag( false )
{
}

bool SpinLock::check() const
{
    return flag.load();
}

void SpinLock::acquire()
{
    bool expected( false );
    while( !flag.compare_exchange_weak( expected, true, std::memory_order_acquire ) )
    {
        expected = false; // acquire lock
    }
}

void SpinLock::release()
{
    flag.store( false, std::memory_order_release );
}

}
