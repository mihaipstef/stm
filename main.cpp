#include <iostream>
#include <vector>

#include "var.hpp"
#include "stm.hpp"
#include "test.hpp"

std::vector<util::Test> tests
{
    TEST(1) /*Test no. 1 - One shared variable*/
    {
        //Declare and init shared variables
        stm::Var<int> v1( 0 );
        int N(1250);
        int Nths( 8 );

        TEST_CODE /*Increment atomically a shared variable N times*/
        {
            for( int i = 0; i < N; ++i )
            {
                TRANSACTION(t,&v1)
                {
                    v1.store( 1 + v1.load(t), t );
                };
            }
        };

        VALIDATION_CODE
        {
            return ( Nths * N ) == v1.getValue();
        };

        RESULT_OF_TEST(Nths) /*Get the result of the test executed on Nths threads*/
    },
    TEST(2) /*Test no. 2 - Two shared variables*/
    {
        //Declare and init shared variables
        stm::Var<int> v1( 0 );
        stm::Var<double> v2( 0 );
        int N(1250);
        int Nths( 8 );

        TEST_CODE /*Increment atomically two shared variables N times, one depending on the other*/
        {
            for( int i = 0; i < N; ++i )
            {
                TRANSACTION(t,&v1,&v2)
                {
                    int v1Local = v1.load(t) + 1;
                    v2.store( v1Local + v2.load(t), t );
                    v1.store( v1Local, t );
                };
            }
        };

        VALIDATION_CODE
        {
            return ( ( Nths * N ) == v1.getValue() ) && ( ( N * Nths* (Nths * N + 1) / 2 ) == v2.getValue() );
        };
        RESULT_OF_TEST(Nths) /*Get the result of the test executed on Nths threads*/
    } };

int main()
{
    for( auto&& test : tests )
    {
        auto result = test();
        std::string result_msg = ( std::get < 1 > ( result ) ) ? "Passed" : "Failed";
        std::cout << "Test no. " << std::get < 0 > ( result ) << ": " << result_msg << std::endl;
    }
}
