CC=g++
CFLAGS=-c -Wall -std=c++14
LDFLAGS=

ifeq ($(OS),Windows_NT)
	EXECUTABLE=stm.exe
else
	LDFLAGS+=-pthread
	EXECUTABLE=stm
endif

OUTPUT_DIR=out
SRC_DIR=.
STM_LIB_DIR=lib
STM_INCLUDE_DIR=$(STM_LIB_DIR)/include

SOURCES=$(wildcard $(STM_LIB_DIR)/*.cpp)
SOURCES+=$(wildcard *.cpp)
INCLUDE=$(STM_INCLUDE_DIR)

$(OUTPUT_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CFLAGS) -c -o $@ $^

OBJECTS=$(patsubst %.cpp, $(OUTPUT_DIR)/%.o, $(SOURCES))

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(LDFLAGS) $(OBJECTS) -o $(OUTPUT_DIR)/$@

$(OUTPUT_DIR)/%.o: %.cpp
	echo "Building file: $<"
	$(CC) -I $(INCLUDE) $(CFLAGS) $< -o $@

.PHONY: build
.PHONY: clean
.PHONY: mkdir

build: mkdir $(SOURCES) $(EXECUTABLE)

clean:
	rm -f $(OBJECTS) $(OUTPUT_DIR)/$(EXECUTABLE)

mkdir:
	mkdir -p $(OUTPUT_DIR)
	mkdir -p $(OUTPUT_DIR)/$(STM_LIB_DIR)
