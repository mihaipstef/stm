#include "test.hpp"

#include <future>
#include <vector>

namespace util
{

bool execute_and_validate( int nThs, const TestCodeFn& test_fn, const ValidationCodeFn& validation_fn )
{
    std::vector<std::shared_future<void>> futures;

    for( int th = 0; th < nThs; ++th )
    {
        futures.push_back( std::async( std::launch::async, test_fn ) );
    }

    for( auto f : futures )
    {
        f.wait();
    }
    return validation_fn();
}

}
