/**
 * @file   test.hpp
 * @Author Mihai Stef
 *
 * Contains utilities for testing.
 */

#ifndef TEST_HPP_
#define TEST_HPP_

#include <functional>
#include <tuple>

#define TEST(n) []( const int test_no = n ) -> std::tuple<int,bool>
#define TEST_CODE auto test_code = [&]()
#define VALIDATION_CODE auto validation_code = [&]()
#define RESULT_OF_TEST(n) return std::make_tuple(test_no, \
                        util::execute_and_validate( n, test_code, validation_code ) );

namespace util
{

typedef std::function<void()> TestCodeFn;
typedef std::function<bool()> ValidationCodeFn;
typedef std::function<std::tuple<int, bool>()> Test;

/**
 * Executes the test code on nThs threads and call validation code after.
 */
bool execute_and_validate( int nThs, const TestCodeFn& test_fn, const ValidationCodeFn& validation_fn );

}

#endif /* TEST_HPP_ */
